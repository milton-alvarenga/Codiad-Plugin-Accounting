<?php
    /**
     * @file
     *		controller.php
     * 
     * Accounting Plugin for Codiad
     * 
     * @author
     *		V.Lopez		<v.lopez@caixadesoft.es>
     * 
     * @version
     *		2019.06.21 - Preliminary version
     */

    require_once('../../common.php');
    require_once('class.accounting.php');
    
    $Accounting = new Accounting();
    
	if ($_GET['action']=='save_positions') 
	{
		$Accounting->savePositions( $_SESSION['user'] , $_POST["positions"] );
	}
