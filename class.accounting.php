<?php
	/**
	 * @file
	 *		class.accounting.php
	 * 
	 * Accounting Plugin for Codiad
	 * 
	 * @author
	 *		V.Lopez		<v.lopez@caixadesoft.es>
	 * 
	 * @version
	 *		2019.06.21 - Preliminary version
	 * 
	 */

	require_once('../../common.php');

	/**
	 * Accounting
	 */
	class Accounting extends Common 
	{
		/**
		 * Send positions to DB.
		 * 
		 * Create a new register for each file, if position of the cursor changes
		 * adds the interval time
		 */
		public function savePositions( $user , $positions ) 
		{
			/**
			 * DB Object
			 */
			global $sql;
			
			$positions = json_decode( $positions, true );
	
			$query = "";
			$bind_variables = array();
			
			if( json_last_error() == JSON_ERROR_NONE ) 
			{
				foreach ( $positions as $path => $cursor ) 
				{
					$query.= "INSERT INTO file_usage ( username , path , position , last_access , time ) VALUES ( ? , ? , ? , ? , ? ) ";
					$query.= "ON DUPLICATE KEY UPDATE ";
					$query.= "time = IF( position <> ? , time + 10 , time );";
					
					array_push( $bind_variables , $user , $path , json_encode( $cursor ) , time() , 0 , json_encode( $cursor ) );
				}
				
				$return = $sql->query( $query , $bind_variables , 0 , "rowCount" );
			}

			exit( formatJSEND( "success" ) );
		}
	}
