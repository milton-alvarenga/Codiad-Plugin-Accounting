/**
 * @file 
 *		init.js
 * 
 * Accounting Plugin for codiad
 * 
 * @author
 *		V.Lopez		<v.lopez@caixadesoft.es>
 * 
 * @version
 *		2019.06.21 - Preliminary version
 */

( function( global, $ ) {

	var codiad = global.codiad;
	var scripts= document.getElementsByTagName('script');
	var path = scripts[scripts.length-1].src.split('?')[0];
	var curpath = path.split('/').slice(0, -1).join('/')+'/';

	// --- Start Plugin ---
	$( function() { 
		codiad.accounting.init();
	});
	
	codiad.accounting = {
				controller: curpath + 'controller.php',

				poller: null,
				interval: 10000,
				
				init: function() {
						let _this = this;
					
						_this.poller = setInterval( function() {
													codiad.accounting.uploadPositions();
												} , _this.interval );
					},
		
				uploadPositions: function() {
						$.ajax({
								type: 'POST',
								url: codiad.accounting.controller + '?action=save_positions',
								data: {
										positions: ( JSON.stringify( codiad.active.positions ) )
									},
								success: function( data ) {	},
							});
					}
	}
})( this, jQuery );

