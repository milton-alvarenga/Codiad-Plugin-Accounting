# Accounting Codiad Plugin 

Plugin for accounting file usage per user.

Need a table in a database with these fields:

```
CREATE TABLE `file_usage` (
  `username` char(50) COLLATE utf8_bin NOT NULL,
  `path` char(255) COLLATE utf8_bin NOT NULL,
  `position` char(100) COLLATE utf8_bin NOT NULL,
  `last_access` int(10) unsigned NOT NULL,
  `time` int(10) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `username_path` (`username`,`path`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
```

